<?php
include("server.php");

$db = dbConexion();
$query = mysqli_query($db, "SELECT * FROM cliente");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detalle venta</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  </head>
  <body>
		<div class="container">
			<h1 class="h1 text-center text-primary m-5">Lista de clientes</h1>
			<table class="table table-striped text-center">
				<thead>
					<tr>
						<th align="center">Nro cliente</th>
						<th align="center">Nombre cliente</th>
						<th align="center">Edad cliente</th>
						<th align="center">Altura cliente</th>
						<th align="center">Peso Cliente</th>
						<th align="center">IMC cliente</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($row = mysqli_fetch_object($query)):

					?>
						<tr>
							<td align="center">
								<?php echo $row->idCliente;?>
                                     </td>
                                           <td align="center">
								<?php echo $row->nombreCliente;?>
							</td>

							<td align="center">
								<?php echo $row->edadCliente;?>
							</td>
							<td align="center">
								<?php echo $row->alturaCliente;?>
							</td>
							<td align="center">
								<?php echo $row->pesoCliente;?>
							</td>
							<td align="center">
								<?php echo $row->imcCliente; ?>
							</td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
		<p class="text-center">
			<a href="./index.html">Agregar otro cliente</a>
		</p>
  </body>
</html>
